History Department Theme
========================

This subtheme will make modifications to a standard theme in Open Scholar.

This subtheme is based on the [Open Scholar starter kit](https://github.com/openscholar/starterkit)
